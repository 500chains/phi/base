# Query a session
This query allows you to retrieve the details of a session having its id. 

**CLI**
```bash
phicli query sessions session [id]

# Example
# desmosli query sessions session 66
```

**REST**
```
/sessions/{sessionId}

# Example
# curl http://lcd.morpheus.phi.network:1317/sessions/66
```
