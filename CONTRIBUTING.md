# We need your help

You can contribute in the following ways:

## Submit issues
If you find any errors, bugs, questions, feature requests when you are running the Phi, please do not hestitate to submit any [issues](https://gitlab.com/500chains/phi/base/issues). Any bug report and comments are welcome. We always want to know how to improve a decentralized social network.

## Create Pull Request
If you are generous enough and can resolve some issues Phi is experiencing, please build everything in your own branch and create a Pull Request to us. We will review and merge if necessary.

If it's just some simple errors like typo or missing a variable, etc, please create a Pull Request directly. If it's a feature change or some severe issues, please create an issue before creating a Pull Request.
