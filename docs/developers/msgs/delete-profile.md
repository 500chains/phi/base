# `MsgDeleteProfile`
This message allows you to delete a previously created profile.

## Structure
````json
{
  "type": "phi/MsgDeleteProfile",
  "value": {
    "creator": "<Address of the profile owner>"
  }
}
````

### Attributes
| Attribute | Type | Description |
| :-------: | :----: | :-------- |
| `creator` | String | Phi address of the user that is deleting the profile |

## Example
````json
{
  "type": "phi/MsgDeleteProfile",
  "value": {
    "creator": "desmos1qchdngxk8zkl4c4mheqdlpgcegkdrtucmwllpx"
  }
}
````

## Message action
The action associated to this message is the following:

```
delete_profile
```
