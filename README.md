# phi/base

[![GoDoc](https://img.shields.io/badge/godoc-documentation-blue.svg)](https://godoc.org/github.com/p9c/pod) 
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/500chains/phi/base)](https://goreportcard.com/report/gitlab.com/500chains/phi/base)

500chains official base cryptocurrency and testbed for custom module development.
