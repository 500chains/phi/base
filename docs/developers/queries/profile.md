# Query a profile
This query endpoint allows you to retrieve the details of a single profile having its moniker or address. 

**CLI**
 ```bash
phicli query profiles profile [address_or_moniker]

# Example
# phicli query profiles profile desmos12a2y7fflz6g4e5gn0mh0n9dkrzllj0q5vx7c6t
# phicli query profiles profile leonardo
``` 

**REST**
```
/profiles/{address_or_moniker}

# Example
# curl http://lcd.morpheus.phi.network:1317/profiles/desmos12a2y7fflz6g4e5gn0mh0n9dkrzllj0q5vx7c6t
# curl http://lcd.morpheus.phi.network:1317/profiles/leonardo
```
