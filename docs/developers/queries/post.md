# Query a post
This query endpoint allows you to retrieve the details of a single post having its id. 

**CLI**
 ```bash
phicli query posts post [id]

# Example
# phicli query posts post a4469741bb0c0622627810082a5f2e4e54fbbb888f25a4771a5eebc697d30cfc
``` 

**REST**
```
/posts/{postId}

# Example
# curl http://lcd.morpheus.phi.network:1317/posts/a4469741bb0c0622627810082a5f2e4e54fbbb888f25a4771a5eebc697d30cfc
```
