module gitlab.com/500chains/phi/base

go 1.13

require (
	gioui.org/cmd v0.0.0-20200704155525-7bbe0da0c75f // indirect
	gioui.org/example v0.0.0-20200704155525-7bbe0da0c75f // indirect
	github.com/cosmos/cosmos-sdk v0.38.4
	github.com/otiai10/copy v1.0.2
	github.com/spf13/cobra v0.0.6
	github.com/spf13/viper v1.6.2
	github.com/stretchr/testify v1.5.1
	github.com/tendermint/go-amino v0.15.1
	github.com/tendermint/tendermint v0.33.3
	github.com/tendermint/tm-db v0.5.0
	golang.org/x/net v0.0.0-20200202094626-16171245cfb2 // indirect
)
