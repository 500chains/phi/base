# Query posts module parameters
This query endpoint returns all the parameters of the `posts` module.

**CLI**
 ```bash
phicli query posts params

# Example
# phicli query posts params
``` 
**REST**
```
/posts/params

# Example
# curl http://lcd.morpheus.phi.network:1317/posts/params
``` 

# Query profiles module parameters
This query endpoint returns all the parameters of the `profiles` module.

**CLI**
 ```bash
phicli query profiles params

# Example
# phicli query profiles params
``` 
**REST**
```
/profiles/params

# Example
# curl http://lcd.morpheus.phi.network:1317/profiles/params
```  

