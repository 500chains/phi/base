# Query the stored profiles
This query endpoint allows you to get all the stored profiles.

**CLI**
 ```bash
phicli query profiles all

# Example
# phicli query profiles all
``` 

**REST**
```
/profiles

# Example
# curl http://lcd.morpheus.phi.network:1317/profiles
``` 

# Query a profile with the given moniker
This query endpoint allows you to get the profile related to the given `moniker`.

**CLI**
 ```bash
phicli query profiles profile bob

# Example
# phicli query profiles profile bob
``` 
**REST**
```
/profiles/{address_or_moniker}

# Example
# curl http://lcd.morpheus.phi.network:1317/profiles/bob
``` 
