|[500chains](https://gitlab.com/500chains)|[phi](https://gitlab.com/500chains/phi)|[base](https://gitlab.com/500chains/phi/base)|testnet|
|---|---|---|---|

# contents

1. [how to make a new test network](#newtestnetwork)<!-- @IGNORE PREVIOUS: anchor -->
1. [new test network script](#newtestnetworkscript)<!-- @IGNORE PREVIOUS: anchor -->

---

## <a name="newtestnetwork">1. how to make a new test network</a>

you can run all of these commands from your home directory

```bash
cd $HOME
```

initialize the genesis.json file that will help you to bootstrap the network

create a key to hold your validator account

```bash
export USER_NAME=alice
phicli keys add $USER_NAME
```

add that key into the genesis.app_state.accounts array in the genesis file

```bash
export CHAIN=ABC-widget-chain
phid init --chain-id=$CHAIN $USER_NAME
```

this command lets you set the number of coins.

make sure this account has some coins
with the `genesis.app_state.staking.params.bond_denom` denom, the default is staking

```bash
phid add-genesis-account $(phicli keys show $USER_NAME -a) \
   1000000000stake
```

generate the transaction that creates your validator

```bash
phid gentx --name $USER_NAME
```

add the generated bonding transaction to the genesis file

```bash
phid collect-gentxs
```

now its safe to start `phid`


```bash
phid start
```

---

## <a name="newtestnetworkscript">2. new test network script</a>

assuming you have a `$GOBIN` set (if not, put one in your environment's shell profile startup script eg `.bashrc`)

```bash
nano $GOBIN/new_chain && chmod +x $GOBIN/new_chain
```

make a file containing the following

```bash
#!/bin/bash
echo setting up a new chain
echo
echo ensure your EDITOR variable is set to your favourite editor
echo
echo typical example for bash and nano:
echo \ \ \ \ export EDITOR="\$(which nano)" \>\> ~/.bashrc
echo;read -rsn1 -p"ctrl-c to stop, enter to continue";echo
CONFFILE=$HOME/.phi_env_tmp
echo \# new_chain > $CONFFILE
echo \# configure a new chain by setting the variables below, close and save >> $CONFFILE
echo PHIHOME=~/.phi >> $CONFFILE
echo CHAIN=t-chain >> $CONFFILE
echo USER_NAME=alice >> $CONFFILE
echo STAKE=stake >> $CONFFILE
echo TOKEN=fives >> $CONFFILE
$EDITOR $CONFFILE && \
echo using the following settings:
echo
cat $CONFFILE|grep -v ^#
echo;read -rsn1 -p"ctrl-c to stop, enter to continue";echo
source $CONFFILE
CLIHOME=$PHIHOME/cli
echo initializing chain $CHAIN for validator $USER_NAME && \
phid init --chain-id=$CHAIN $USER_NAME --home "$PHIHOME" && \
echo adding key for $USER_NAME && \
phicli keys add $USER_NAME --home "$CLIHOME" && \
echo adding genesis account denoms: stake=$STAKE token=$TOKEN && \
phid add-genesis-account $(phicli keys show $USER_NAME -a --home \
    "$CLIHOME") 1000000000$STAKE,1000000000$TOKEN --home \
    "$PHIHOME" && \
echo generating transaction && \
phid gentx --name $USER_NAME --home "$PHIHOME" && \
echo collecting generated transactions && \
phid collect-gentxs  --home "$PHIHOME" && \
echo starting phi daemon && \
phid start --home "$PHIHOME" && \
echo finished && \
rm $CONFFILE
```


call it like this:

```bash
scripts/new_chain
```

the script above makes it simple just assuming one variable is set and the rest should be intuitive to the user

this script can be found in the scripts folder at the root of the repository
