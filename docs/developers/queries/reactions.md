# Query registered reactions
This query endpoint allows you to retrieve the list of registered reactions. 

**CLI**
 ```bash
phicli query posts registered-reactions

# Example
# phicli query posts registered-reactions
``` 

**REST**
```
/reactions

# Example
# curl http://lcd.morpheus.phi.network:1317/reactions
```
