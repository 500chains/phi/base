# Installing Phi
Phi is actually composed of two different executables: `phicli` and `phid`.

The first one, `phicli`, represents the Command Line Interface (CLI) that you can use to interface with the Phi blockchain.

The second one, `phid` (a.k.a. Phi Daemon), represents instead the executable that allows you take part to a Phi blockchain either as a full node or a [validator node](validators/overview.md).

## Installing the Phi CLI and Daemon
All you have to do to install both the `phicli` and `phid` executable is following the below procedure.

### Requirements
The requirements you must satisfy before attempting to install Phi CLI and Daemon are the following:

- Having Go 1.13 or later installed.  
   If you dont have it, you can download it [here](https://golang.org/dl/).
   
- Having Git installed.  
  If you need to install it, you can download the installer on the [official website](https://git-scm.com/downloads).
   
### Installation procedure 
To install `phicli` and `phid` execute the following commands:

```bash
cd /home/$USER
git clone https://gitlab.com/500chains/phi/base && cd phi
make install
```

### Verify the installation 
To verify you have correctly installed `phicli` and `phid`, try running:

```bash
phicli version
phid version
``` 

If you get an error like `No command found`, please make sure you have appended your `GOBIN` folder path to your system's `PATH` environmental variable value.    

:::tip Congratulations!   
You have successfully installed `phicli` and `phid`!  
:::
