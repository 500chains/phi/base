# Join the public testnet

:::tip Current Testnet  
See the [testnet repo](https://github.com/phi-labs/morpheus) for information on the latest testnet, including the correct version of Phi to use and details about the genesis file.  
:::

## Validators
:::warning Requires phi executables  
To join the public testnet you **must** have [`phicli` and `phid` installed](/fullnode/installation.md).  
:::

To become a testnet validators, the mainnet instructions apply: 

1. [Create a full node](../fullnode/installation.md).
2. [Become a validator](../validators/setup.md)

The only difference is the Phi software version and genesis file. See the [testnet repo](https://github.com/phi-labs/morpheus) for information on testnets, including the correct version of Phi to use and details about the genesis file.



